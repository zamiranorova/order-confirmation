require 'spec_helper'

describe 'Parse OC' do
  describe '.perform!' do
    let(:pdf_file) { 'sample_order_confirmation.pdf' }
    let(:none_pdf_file) { 'not_a_pdf.csv' }
    #let(:incomplete_file) { 'incomplete_file.pdf' }

    it 'works' do
      type, message = ParseOc.new(pdf_file).perform!
      expect(type).to eq :success
      expect(message).to eq 'Order confirmation parsed successfully'
      expect(Order.where(order_number: '12345678')).to exist

      order = Order.find_by(order_number: '12345678')
      expect(order.vendor_name).to eq 'PURCHASING PLATFORM    LLC'
      expect(order.po_number).to eq '22551-1'

      type, message = ParseOc.new(none_pdf_file).perform!
      expect(type).to eq :danger
      expect(message).to eq 'Not a PDF file'

      #type, message = ParseOc.new(incomplete_file).perform!
      #expect(type).to eq :danger
      #expect(message).to eq "Validation failed: Total can't be blank, Total is not a number"
    end
  end
end
