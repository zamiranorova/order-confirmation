class LineItem < ActiveRecord::Base
  belongs_to :order

  validates :line_number, :item_number, :description, presence: true
  validates :quantity, :unit_price, presence: true
  validates :line_number, numericality: { greater_than: 0, only_integer: true },
    uniqueness: {scope: :order_id, message: 'line number is already in use'}
  validates :item_number, uniqueness: {scope: :order_id, message: 'item number is already in use'}
  validates :quantity, numericality: { greater_than: 0, only_integer: true }
  validates :unit_price, numericality: { greater_than: 0 }
end
