class Order < ActiveRecord::Base
  enum status: {
    pending:    'pending',
    processing: 'processing',
    success: 'success',
    rejected: 'rejected'
  }

  has_many :line_items, dependent: :destroy

  validates :confirmed_on, :vendor_name, :order_number, :status, presence: true
  validates :po_number, :total, presence: true
  validates :total, numericality: { greater_than: 0 }
  validates :order_number, uniqueness: true
end
