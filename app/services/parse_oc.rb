class ParseOc
  def initialize tempfile
    @tempfile = tempfile
  end

  def perform!
    raise 'Not a PDF file' unless File.extname(@tempfile) == '.pdf'
    raise 'Cannot open file' unless pdf_io = File.open(@tempfile)

    reader = PDF::Reader.new pdf_io
    page = reader.pages.first.text
    order = Order.new

    ActiveRecord::Base.transaction do
      _, vendor_name, order_number = page.match(/^CUSTOMER NAME : (\w*\W+\s*.*)ORDER NUMBER : (\w*)/).to_a
      _, _, date = page.match(/^(.*\S*\s*\d*\w*)DATE ENTERED : (\d*.*)/).to_a
      _, _, time = page.match(/^(.*\S*\s*\d*\w*)TIME ENTERED : (\d*.*)/).to_a
      _, _, status = page.match(/^(.*\S*\s*\d*\w*)STATUS : (\w*)/).to_a
      _, _, order.po_number = page.match(/^(.*\S*\s*\d*\w*)PURCHASE ORDER NUMBER : (.*)/).to_a
      _, _, order.total = page.match(/^(.*\S*\s*\d*\w*)NET\s* (.*)/).to_a

      order.vendor_name = vendor_name.strip
      order.order_number = order_number.strip
      order.status = status.downcase
      order.confirmed_on = DateTime.strptime("#{date} #{time}", "%m/%d/%y %H:%M")
      order.save!

      _, items = page.match(/-{10}\n\s(.*)\n\s{20,}-{7}/m).to_a
      items = items.split("\n\n ")

      items.each do |item|
        line_item = order.line_items.new
        line_item.line_number = item[0..2].strip
        line_item.item_number = item[4..16].strip
        line_item.description = item[18..46].strip
        line_item.quantity = item[48..58].strip
        line_item.unit_price = item[59..68].strip
        line_item.save!
      end

      raise StandartError unless order.reload.line_items.count > 0
    end

    [:success, 'Order confirmation parsed successfully']
  rescue => e
    [:danger, e.message]
  end
end
