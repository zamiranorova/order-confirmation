class OrdersController < ApplicationController
  def index
    @orders = Order.all
  end

  def show
    order = Order.find params[:id]

    render json: { data: OrderSerializer.new(order).build_schema }
  end

  def parse_oc
    tempfile = params[:order][:order_confirmation_pdf].tempfile.path

    type, message = ParseOc.new(tempfile).perform!

    flash[type] = message

    redirect_to root_path, type => message
  end
end
