class LineItemSerializer < Surrealist::Serializer

  json_schema do
    {
      line_number: Integer,
      item_number: String,
      description: String,
      quantity: Integer,
      unit_price: Float
    }
  end

  def unit_price
    object.unit_price.to_f
  end
end
