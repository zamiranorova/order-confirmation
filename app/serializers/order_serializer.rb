class OrderSerializer < Surrealist::Serializer

  json_schema do
    {
      confirmed_on: DateTime,
      vendor_name: String,
      order_number: String,
      status: String,
      po_number: String,
      total: Float,
      line_items: Array
    }
  end

  def total
    object.total.to_f
  end

  def confirmed_on
    object.confirmed_on.to_datetime
  end

  def line_items
    object.line_items.map do |item|
      LineItemSerializer.new(item).build_schema
    end
  end
end
