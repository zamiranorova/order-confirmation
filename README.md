**Description**

Decided it is a good opportunity to get to know to rails 6, so it is. 
PostgreSQL because I dont have any more DBMS. Sorry. 

It is a test project, so i commited master.key to repo. Otherwise you could not run the project. 

`localhost:3000` gets you to page of all order confirmations with field to upload file for parsing. Each row has a link to details hash in json format.

Interesting things to check: migrations, models, controllers, serializers, services, specs. 

---

## Prerequisitions

* Rails 6.0.0 (+ yarn, node 10 or higher, webpack)
* Ruby 2.6.2
* PostgreSQL 9.6.14

---

## How to run

* `clone git@bitbucket.org:zamiranorova/order-confirmation.git`
* `cp config/database.yml.sample config/database.yml`
* `rake db:create`
* `rails s`
