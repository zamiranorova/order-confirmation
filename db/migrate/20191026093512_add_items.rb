class AddItems < ActiveRecord::Migration[6.0]
  def change
    create_table :line_items do |t|
      t.references :order, foreign_key: false
      t.integer    :line_number, null: false
      t.string     :item_number, null: false
      t.string     :description, null: false
      t.integer    :quantity, null: false
      t.decimal    :unit_price, precision: 10, scale: 2, null: false

      t.timestamps null: false
    end
  end
end
