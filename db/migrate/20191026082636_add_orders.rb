class AddOrders < ActiveRecord::Migration[6.0]
  def up
    execute <<-SQL
      CREATE TYPE order_statuses AS ENUM (
        'pending', 'processing', 'success', 'rejected'
      );
    SQL

    create_table :orders do |t|
      t.datetime :confirmed_on, null: false
      t.string   :vendor_name, null: false
      t.string   :order_number, null: false
      t.column   :status, :order_statuses, null: false
      t.string   :po_number
      t.decimal  :total, precision: 10, scale: 2

      t.timestamps null: false
    end
  end

  def down
    drop_table :orders
    execute <<-SQL
      DROP TYPE order_statuses;
    SQL
  end
end
